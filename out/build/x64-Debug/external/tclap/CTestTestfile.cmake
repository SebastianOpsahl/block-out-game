# CMake generated Testfile for 
# Source directory: C:/Users/Sondre/Dropbox/Prosjekt/Group_24/external/tclap
# Build directory: C:/Users/Sondre/Dropbox/Prosjekt/Group_24/out/build/x64-Debug/external/tclap
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("docs")
subdirs("examples")
subdirs("tests")
subdirs("unittests")
subdirs("include/tclap")
